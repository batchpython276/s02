
while True:
    years = input("Enter a year: ")
    if not years.isnumeric():
        print("Invalid input! Please enter a valid number.")
    else:
        year = int(years)
        if year <= 0:
            print("Invalid input! Please enter a positive number.")
        else:
            break


if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
    print(year, "is a leap year")
else:
    print(year, "is not a leap year")


while True:
    row = input("Enter the number of rows: ")
    col = input("Enter the number of columns: ")
    if not row.isnumeric() or not col.isnumeric():
        print("Invalid input! Please enter valid number.")
    else:
        row = int(row)
        col = int(col)
        if row <= 0 or col <= 0:
            print("Invalid input! Please enter positive number.")
        else:
            break

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()
