# Python also allows user to input, with this users can give inputs to the program.

# [Section] input
# username = input("Please enter your name:\n")

# print(f"Hello {username}! Welcome to Python short course!")

# num1 = int(input("Enter First number: "))
# num2 = int(input("enter Second number: "))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# [Section] with user inputs, users can be give inputs for the program to be used to control the app using control structures
# Control Structures can be divided into selcetion and repitition control structures

# Selection control structures allows the program to choose among choices and run specific codes depending on the choice taken(conditions)

# Repitition control structures allows the program to repeat certain blocks of code given a starting condition and termination condition


# [Section] if-else statements
# if else statements are used to choose between two or more code blocks depending on the condition

test_num = 75
if test_num >= 60:
	print("Test passed!")
else:
	print("Test failed!")

# Note that in Python, curly braces ({}) are not needed to distinguish the code blocks inside the if or else block. Hence, indentations are important as python uses indentation to distinguish parts of code as needed.

# [Section] if else chains can also be used to have more than 2 choices for the program

# test_num2 = int(input("Please enter the second number \n"))

# if test_num2 > 0:
# 	print("The number is positive!")
# elif test_num2 == 0:
# 	print("The number is equal to 0!")
# else:
# 	print("The number is negative!")

# test = int(input("Please Enter a number: \n"))


# if test % 3 == 0 and test % 5 == 0:
#     print("The number is divisible by both 3 and 5")
# elif test % 3 == 0:
#     print("The number is divisible by 3")
# elif test % 5 == 0:
#     print("The number is divisible by 5")
# else:
#     print("The number is not divisible by either 3 or 5")


# [Section] Loops
# Python has loops that can repeat blocks of codes
# While loops are used to execute a set of statements as long as the condition is true


# i = 0

# while i <= 5:
# 	# i += 1
# 	print(f"Current value of i is {i}")
# 	i += 1


# [Section] for loops are used for iteration over a sequence

fruits = ["apple", "banana", "cherry"] #lists

for indiv_fruit in fruits:
	print(indiv_fruit)


fruitas = ["gundam", "taken", "anoyan"] #lists

for kala in fruitas:
	print(kala)


# [Section] range() method
# To use the for loop iterate through values, the range method can be used

for x in range(6, 20, 3):
	print(f"the current value of x is {x}!")



for x in range(6, 10):
	print(f"the current value of x is {x}!")



# [Section] Break Statement

# j = 1
# while j < 6 :
# 	print(j)
# 	if  j==5:
# 		break
# 	j+=1

# [Section] Continue Statement
# The continue statement in returns the control to the beginning of the while loop and cont to the next iteration

k = 1
while k < 6 :
	k += 1
	if k==3:
		continue
	print(k)